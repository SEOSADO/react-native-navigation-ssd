# react-native-navigation-ssd

## Getting Started

### Installing

```json
"react-native-navigation-ssd": "https://gitlab.com/SEOSADO/react-native-navigation-ssd.git"
```

### npm install (추후 제거 예정)
``` npm
npm install @react-navigation/native
npm install @react-navigation/stack
npm install react-native-screens
npm install react-native-gesture-handler
npm install react-native-safe-area-context
```

#### 1. android/app/src/main/java/<your package name>/MainActivity.java 추가
```java
// Top Add
import android.os.Bundle;

// Class In Add
@Override
protected void onCreate(Bundle savedInstanceState) {
super.onCreate(null);
}
```

#### 2. 최상위 index.js 추가
```java
import 'react-native-gesture-handler';
```

## Sample Code

```javascript
const App = () => {
  const HomeScreen = ({navigation, route}) => {
    return <View></View>;
  };

  const CustomNavigation = () => {
    const screenFormat = [
      {
        name: 'screenName1',
        component: HomeScreen,
        options: {
          title: 'screenName1',
          headerShown: true,
        },
        initialParams: {test: 1},
      },
      {
        name: 'screenName2',
        component: HomeScreen,
        options: {
          title: 'screenName2',
          headerShown: true,
        },
        initialParams: {test: 2},
      },
      {
        name: 'screenName3',
        component: HomeScreen,
        options: {
          title: 'screenName3',
          headerShown: true,
        },
        initialParams: {test: 3},
      },
    ];
    return (
      <NavigationContainerSSD
        initialRouteName={'screenName1'}
        screenData={screenFormat}
      />
    );
  };

  return <CustomNavigation />;
};

export default App;
```

| **Name**         | **Type**      | **Default** | **Description**  |
| ---------------- | ------------- | ----------- | ---------------- |
| screenData       | array(object) |             | Sample Code 참조 |
| screenOption     | object        |             | Sample Code 참조 |
| modalData        | array(object) |             | Sample Code 참조 |
| modalOption      | object        |             | Sample Code 참조 |
| initialRouteName | string        |             | 시작 screen 이름 |
