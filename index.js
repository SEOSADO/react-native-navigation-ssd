import React, {useEffect, useRef, useState} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import * as navigationStack from '@react-navigation/stack'

const Stack = navigationStack.createStackNavigator();

export const NavigationContainerSSD = ({
  screenData = [],
  screenOption = {},
  modalData = [],
  modalOption = {},
  initialRouteName,
  interpolator
}) => {
  let cardStyleInterpolator = navigationStack.CardStyleInterpolators.forBottomSheetAndroid
  switch(interpolator){
    case 'forBottomSheetAndroid':
      cardStyleInterpolator = navigationStack.CardStyleInterpolators.forBottomSheetAndroid
      break;
    case 'forFadeFromBottomAndroid':
      cardStyleInterpolator = navigationStack.CardStyleInterpolators.forFadeFromBottomAndroid
      break;
    case 'forFadeFromCenter':
      cardStyleInterpolator = navigationStack.CardStyleInterpolators.forFadeFromCenter
      break;
    case 'forHorizontalIOS':
      cardStyleInterpolator = navigationStack.CardStyleInterpolators.forHorizontalIOS
      break;
    case 'forModalPresentationIOS':
      cardStyleInterpolator = navigationStack.CardStyleInterpolators.forModalPresentationIOS
      break;
    case 'forNoAnimation':
      cardStyleInterpolator = navigationStack.CardStyleInterpolators.forNoAnimation
      break;
    case 'forRevealFromBottomAndroid':
      cardStyleInterpolator = navigationStack.CardStyleInterpolators.forRevealFromBottomAndroid
      break;
    case 'forScaleFromCenterAndroid':
      cardStyleInterpolator = navigationStack.CardStyleInterpolators.forScaleFromCenterAndroid
      break;
    case 'forVerticalIOS':
      cardStyleInterpolator = navigationStack.CardStyleInterpolators.forVerticalIOS
      break;
  }
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName={initialRouteName}
        screenOptions={screenOption,{cardStyleInterpolator}}>
        {/* Screen */}
        <Stack.Group>
          {screenData.map((item, index) => (
            <Stack.Screen
              key={index}
              name={item.name}
              component={item.component}
              options={item.options}
              initialParams={item.initialParams}
            />
          ))}
        </Stack.Group>
        {/* Modal */}
        <Stack.Group
          screenOptions={{
            presentation: 'modal',
            modalOption,
          }}>
          {modalData.map((item, index) => (
            <Stack.Screen
              key={index}
              name={item.name}
              component={item.component}
              options={item.options}
              initialParams={item.initialParams}
            />
          ))}
        </Stack.Group>
      </Stack.Navigator>
    </NavigationContainer>
  );
};
