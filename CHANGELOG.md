## 0.0.2

##### Code fix

-   CardStyleInterpolators Add!
-   @react-navigation/native-stack -> @react-navigation/stack Npm Change

---

## 0.0.1

##### New

-   react-native-navigation-ssd

---
